package com.ttn.bootcamp;

import java.util.ArrayList;
import java.util.List;

public class StaticInnerTest {

    public static void main(String[] args) {
        MyMap map = new MyMap();
        map.set("1", "One");
        map.set("2", "two");
        map.set("3", "three");
        map.set("4", "four");
        map.set("5", "five");
        map.set("6", "six");
        map.set("7", "seven");

        List<MyMap.Node> nodeList = map.getNodeList();
        for (MyMap.Node node : nodeList) {
            System.out.println("key : " + node.getKey() + ", value : " + node.getValue());
        }
    }

}


class MyMap {

    private List<Node> nodeList = new ArrayList<>();
    private String name;

    public void set(String key, String value) {
        boolean flag = false;
        for (Node node : nodeList) {
            if (node.key.equals(key)) {
                node.value = value;
                flag = true;
                break;
            }
        }
        if (!flag) {
            Node node = new Node(key, value);
            nodeList.add(node);
        }
    }

    public String get(String key) {
        for (Node node : nodeList) {
            if (node.key.equals(key)) {
                return node.value;
            }
        }
        return null;
    }

    public List<Node> getNodeList() {
        return new ArrayList<>(nodeList);
    }

    static class Node {
        private String key;
        private String value;


        public Node(String key, String value) {
            this.key = key;
            this.value = value;
        }

        public String getKey() {
            return key;
        }

        public String getValue() {
            return value;
        }
    }

}