package com.ttn.bootcamp;

public class NewBankAccount extends BankAccount {
    private boolean smsService;
    private String mobileNo;

    public NewBankAccount(String name) {
        super(name);
        this.smsService = false;
    }

    public NewBankAccount(String name, String mobileNo) {
        super(name);
        this.smsService = true;
        this.mobileNo = mobileNo;
    }

    public void activateSmsService(String mobileNo) {
        this.mobileNo = mobileNo;
        this.smsService = true;
    }

    public void deActivateSmsService() {
        this.mobileNo = mobileNo;
        this.smsService = false;
    }


    @Override
    public void deposit(int amount) {
        super.deposit(amount);
        sendSMS(amount + " Rs. deposit in " + this.getName() + " account.");
    }

    @Override
    public void withdrawal(int amount) {
        super.withdrawal(amount);
        sendSMS(amount + " Rs. withdrawal from " + this.getName() + " account.");
    }

    @Override
    public void fundTransfer(BankAccount targetAccount, int amount) {
        super.fundTransfer(targetAccount, amount);
        sendSMS("Successfully fund transfer of " + amount + " Rs. from " + this.getName() + " to " + targetAccount.getName() + " account.");
    }

    @Override
    public int checkBalance() {
        int amount = super.checkBalance();
        sendSMS("Your Account balance is " + amount + " Rs.");
        return amount;
    }

    private void sendSMS(String message) {
        if (this.smsService) {
            System.out.println("********* SMS ***********");
            System.out.println("TO : " + this.mobileNo + "\n" + message);
        }
    }
}