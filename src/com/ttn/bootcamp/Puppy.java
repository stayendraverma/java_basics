package com.ttn.bootcamp;

public class Puppy {
   int puppyAge;

   static {
      System.out.println("I am in static block.");
   }

   {
      System.out.println("I am in instance block 1.");
   }

   public Puppy(String name) {
      // This constructor has one parameter, name.
      System.out.println("Name chosen is :" + name );
   }

   {
      System.out.println("I am in instance block 2.");
   }

   public void setAge( int age ) {
      puppyAge = age;
   }

   public int getAge( ) {
      System.out.println("Puppy's age is :" + puppyAge );
      return puppyAge;
   }

   public static void main(String []args) {
      /* Object creation */
      System.out.println();
      Puppy myPuppy = new Puppy( "tommy" );

      /* Call class method to set puppy's age */
      myPuppy.setAge( 2 );

      /* Call another class method to get puppy's age */
      myPuppy.getAge( );

      /* You can access instance variable as follows as well */
      System.out.println("Variable Value :" + myPuppy.puppyAge );
   }

   {
      System.out.println("I am in instance block 2.");
   }

   static {
      System.out.println("I am in static block 2.");
   }
}