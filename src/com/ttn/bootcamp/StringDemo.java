package com.ttn.bootcamp;

public class StringDemo {

    public static void main(String args[]) {
        char[] helloArray = {'h', 'e', 'l', 'l', 'o', '.'};
        String helloString = new String(helloArray);
        System.out.println(helloString);

        float floatVar = 1.0f;
        int intVar = 10;
        String stringVar = "abcd";


        String format = "The value of the float variable is " +
                "%f, while the value of the integer " +
                "variable is %d, and the string " +
                "is %s";
        System.out.println(format);

        String fs = String.format(format, floatVar, intVar, stringVar);
        System.out.println(fs);

        System.out.printf(format, floatVar, intVar, stringVar);

        System.out.println();

        String s1 = "abcd1234";
        //String s2 = s1.replace("\\d", "");
        String s2 = s1.replace("1234", "");
        String s3 = s1.replaceAll("\\d", "");
        System.out.println("Replace Example1 : " + s2);
        System.out.println("Replace Example2 : " + s3);
    }
}