package com.ttn.bootcamp;

public class MultiThreadingTest {

    public static void main(String[] args) {
        System.out.println("Hi.....");
        Thread hi = new Hi();

        Thread hello = new Hello();
        hi.start();
        hello.start();
    }

}

class Hi extends Thread {
    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println("Hi...");
            try {
                Thread.sleep(1500);
            } catch (InterruptedException ie) {
               ie.printStackTrace();
            }
        }
    }
}

class Hello extends Thread {
    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println("Hello...");
            try {
                Thread.sleep(500);
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            }
        }
    }

}
