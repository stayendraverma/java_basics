package com.ttn.bootcamp;

public class StringCompare {
    public static void main(String[] args) {
        String s1 = "india";
        String s2 = new String("india");

        System.out.println(s1 == s2);
        //System.out.println(s1.equals(s2));

        String s3 = "in" + "dia";
        System.out.println(s3 == s2);
        //System.out.println(s3.equals(s2));

        System.out.println(s3 == s1);
        //System.out.println(s3.equals(s1));

        String s4 = s2.intern();
        System.out.println(s1 == s4);
        //System.out.println(s1.equals(s4));

    }
}
