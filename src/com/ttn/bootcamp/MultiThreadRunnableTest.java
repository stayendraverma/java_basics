package com.ttn.bootcamp;

public class MultiThreadRunnableTest {

    public static void main(String[] args) throws InterruptedException {
        System.out.println("Hi.....");
        Runnable hi = new RunnableHi();
        Runnable hello = new RunnableHello();

        Thread t1 = new Thread(hi);
        Thread t2 = new Thread(hello);
        t1.start();
        t2.start();

        System.out.println("Bye...");
    }

}

class RunnableHi implements Runnable {
    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println("Hi...");
            try {
                Thread.sleep(1500);
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            }
        }
    }
}

class RunnableHello implements Runnable {
    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println("Hello...");
            try {
                Thread.sleep(500);
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            }
        }
    }

}
