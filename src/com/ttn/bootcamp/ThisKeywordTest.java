package com.ttn.bootcamp;

public class ThisKeywordTest {
    public static void main(String[] args) {
        BankAccount sbi = new BankAccount("SBI");
        BankAccount axis = new BankAccount("AXIS");
        sbi.deposit(1000);
        axis.checkBalance();
        sbi.fundTransfer(axis, 200);
        axis.checkBalance();
        //axis.withdrawal(10);
    }
}


