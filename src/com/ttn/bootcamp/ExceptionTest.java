package com.ttn.bootcamp;

public class ExceptionTest {

    public static void main(String[] args) {

        try {
            int x = 5 / 0;
            System.out.println(x);
        } catch (RuntimeException e) {
            System.err.println("exception occurs by " + e.getCause() + ", message : " + e.getMessage() + ", class name " + e.getClass());
            //e.printStackTrace();
        } finally {
            System.out.println("finally block executing");
        }
    }

}
