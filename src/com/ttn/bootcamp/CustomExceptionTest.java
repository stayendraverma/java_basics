package com.ttn.bootcamp;

public class CustomExceptionTest {

    public static void main(String[] args) {
        CustomExceptionTest exceptionTest = new CustomExceptionTest();

        try {
            exceptionTest.invokeMethod1();
        } catch (MyCheckedException ex) {
            System.err.println("");
            ex.printStackTrace();
        }

        exceptionTest.invokeMethod2();
    }

    public void invokeMethod1() throws MyCheckedException {
        System.out.println("I am in invokeMethod1");
        throw new MyCheckedException();
    }

    public void invokeMethod2() {
        System.out.println("I am in invokeMethod2");
        throw new MyUncheckedException();
    }
}

class MyCheckedException extends Exception {

}

class MyUncheckedException extends RuntimeException {
}
