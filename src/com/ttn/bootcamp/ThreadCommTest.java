package com.ttn.bootcamp;

public class ThreadCommTest {
    public static void main(String[] args) {
        Queue queue = new Queue();
        new Thread(new Producer(queue), "Producer1").start();
        new Thread(new Consumer(queue), "Consumer1").start();
//        for (int i = 0; i < 10; i++) {
//            new Thread(new Producer(queue), "Producer" + i).start();
//        }
//        new Thread(new Consumer(queue), "Consumer1").start();
//        new Thread(new Consumer(queue), "Consumer2").start();
//        new Thread(new Consumer(queue), "Consumer3").start();
    }
}

class Queue {
    int num;

    boolean flag = false;

    public synchronized void get() {
        while (!flag) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("value = " + num + " consume by " + Thread.currentThread().getName());
        flag = false;
        notifyAll();
    }

    public synchronized void put(int num) {
        while (flag) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("put : " + num + " produce by " + Thread.currentThread().getName());
        this.num = num;
        flag = true;
        notifyAll();
    }
}

class Producer implements Runnable {

    private Queue queue;

    private static int num = 0;

    public static int getNumber() {
        return ++num;
    }

    Producer(Queue queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        while (true) {
            queue.put(getNumber());
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

class Consumer implements Runnable {
    private Queue queue;

    Consumer(Queue queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        while (true) {
            queue.get();
            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
