package com.ttn.bootcamp;

public class BankAccount {
    private int balance;

    private String name;

    public String getName() {
        return name;
    }

    public BankAccount(String name) {
        this.name = name;
    }

    public void deposit(int amount) {
        if (amount > 0) {
            this.balance += amount;
            System.out.println(amount + " Rs. deposit in " + name + " account.");
        } else {
            throw new RuntimeException("Invalid amount deposited");
        }
    }

    public void withdrawal(int amount) {
        if (balance >= amount) {
            if (amount > 0) {
                this.balance -= amount;
                System.out.println(amount + " Rs. withdrawal from " + name + " account.");
            } else {
                throw new RuntimeException("Invalid amount withdrawal");
            }
        } else {
            throw new RuntimeException("Insufficient balance");
        }
    }

    public void fundTransfer(BankAccount targetAccount, int amount) {
        if (targetAccount != null) {
            this.withdrawal(amount);
            targetAccount.deposit(amount);
            System.out.println("Successfully fund transfer of " + amount + " Rs. from " + this.name + " to " + targetAccount.name + " account.");

        } else {
            throw new RuntimeException("Invalid bank account");
        }
    }


    public int checkBalance() {
        System.out.println("Your Account balance is " + balance + " Rs.");
        return balance;
    }
}