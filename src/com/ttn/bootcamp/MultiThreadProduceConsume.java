package com.ttn.bootcamp;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class MultiThreadProduceConsume {
    public static void main(String[] args) {
        BlockingQueue<Integer> sharedQueue = new LinkedBlockingQueue<>();

        for (int i = 1; i <= 10; i++) {
            new Thread(new AdvProducer(sharedQueue, i), "Producer" + i).start();
        }

        for (int i = 1; i <= 20; i++) {
            new Thread(new AdvConsumner(sharedQueue, i), "Consumer" + i).start();
        }
    }
}


class AdvProducer implements Runnable {

    private BlockingQueue<Integer> queue;
    private int threadNumber;

    AdvProducer(BlockingQueue<Integer> queue, int threadNumber) {
        this.queue = queue;
        this.threadNumber = threadNumber;
    }

    @Override
    public void run() {
        int i = 0;
        while (true) {
            try {
                int number = i + (10 * threadNumber);
                System.out.println("Produced:" + number + ":by thread:" + Thread.currentThread().getName());
                queue.put(number);
                i++;
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

class AdvConsumner implements Runnable {
    private BlockingQueue<Integer> queue;
    private int threadNumber;

    AdvConsumner(BlockingQueue<Integer> queue, int threadNumber) {
        this.queue = queue;
        this.threadNumber = threadNumber;
    }

    @Override
    public void run() {
        while (true) {
            try {
                int num = queue.take();
                System.out.println("Consumed: " + num + ":by thread:" + Thread.currentThread().getName());
                Thread.sleep(2500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
