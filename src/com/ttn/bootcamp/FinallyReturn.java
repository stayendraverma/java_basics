package com.ttn.bootcamp;

public class FinallyReturn {
    public static final void main(String[] args) {
        System.out.println(foo("5"));
    }

    private static int foo(String arg) {
        try {
            int n = Integer.parseInt(arg);
            return n;
        } catch (Exception ex) {
            return 45;
        } finally {
            return 42;
        }
    }
}