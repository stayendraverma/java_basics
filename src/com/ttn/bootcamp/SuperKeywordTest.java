package com.ttn.bootcamp;

public class SuperKeywordTest {

    public static void main(String[] args) {
        BankAccount sbi = new NewBankAccount("SBI","1234567890");
        BankAccount axis = new NewBankAccount("AXIS","9876543210");
        sbi.deposit(1000);
        axis.checkBalance();
        sbi.fundTransfer(axis, 200);
        axis.checkBalance();
    }
}