package com.ttn.bootcamp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class NonStaticInnerTest {

    public static void main(String[] args) {
        Book book = new Book("Java", "Satyendra");
        String[] arr = {"Basic Concept", "Object Oriented Programming", "Exception Handling", "Inner Class", "Multi Threading"};
        book.write(Arrays.asList(arr));
        book.read();

//        class A{
//
//        }
//        System.out.println(new A());
    }
}

class Book {

    private String name;
    private String author;
    private List<Page> pages = new ArrayList<>();

    Book(String name, String author) {
        this.name = name;
        this.author = author;
    }

    public void write(List<String> contentList) {
        for (String content : contentList) {
            pages.add(new Page(content));
        }
    }

    public void read() {
        for (Page page : pages) {
            System.out.println(page.getContent());
        }
    }


    class Page {
        public Page(String content) {
            this.content = content;
        }

        private String content;

        public String getContent() {
            return content + "\n" + name;
        }
    }
}
