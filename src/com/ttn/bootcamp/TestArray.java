package com.ttn.bootcamp;

import java.util.Arrays;

public class TestArray {

    public static void main(String[] args) {

        double type1[] = {1.9, 2.9, 3.4, 3.5};
        double[] type2 = {1.5, 2.5, 3.5, 4.5};
        double[] type3 = new double[]{1.1, 1.2, 1.3, 1.4};
        double[] type4 = new double[2];
        type4[0] = 10.5;
        type4[1] = 9.5;


        System.out.println("*************");
        printArray(type1);
        System.out.println("*************");
        printArray(type2);
        System.out.println("*************");
        printArray(type3);
        System.out.println("*************");
        printArray(type4);

        //Array initialize with predefined fix value
        double[] numberArr = new double[10];
        Arrays.fill(numberArr, 10.9);
        printArray(numberArr);

        System.out.println("length before codyof " + numberArr.length);
        double[] numberArr1 = Arrays.copyOf(type1, 2);
        System.out.println("length after codyof " + numberArr1.length);
        printArray(numberArr1);

        Arrays.sort(type2);
        double[] aa = Arrays.copyOfRange(numberArr, 0, 100);
        System.out.println("length after copyOfRange " + aa.length);
        printArray(aa);

    }

    public static void printArray(double[] myList) {
        // Print all the array elements
        for (int i = 0; i < myList.length; i++) {
            System.out.println(myList[i] + " ");
        }

    }
}