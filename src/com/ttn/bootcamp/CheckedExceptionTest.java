package com.ttn.bootcamp;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class CheckedExceptionTest {

    public static void main(String[] args) {
        try {
            File file = new File("/home/satyendra/Desktop/bootcamp/samplefile.txt");
            FileReader fr = new FileReader(file);
            int i;
            while ((i = fr.read()) != -1)
                System.out.print((char) i);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
