package com.ttn.bootcamp;

public class DaemonThreadTest {

    private static int i;

    public static void main(String[] args) {
        Thread dt = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    System.out.println("Daemon Thread...." + (++i));
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        dt.setDaemon(true);
        dt.start();


        Thread ot = new Thread(new Runnable() {
            @Override
            public void run() {
                int x=0;
                while (x<10) {
                    System.out.println("User Thread...." + (++i));
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    x++;
                }
            }
        });

        ot.start();
    }
}
